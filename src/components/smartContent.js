import React, { useState, useEffect } from "react";

import axios from "axios";
import "./smartcontent.css";
// import { Tooltip } from "bootstrap";

import { TaskItem } from "./taskItem";
import { Akordion } from "./akordion";

export const SmartContent = (props) => {

  // const footerURL = 'img/bg-footer.png'
  const myFooter = {
    backgroundImage: `url("img/bg-footer.png")`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'cover'
  }

  const [post, setPost] = React.useState(null);
  const [email, setEmail] = React.useState("");
  const [name, setName] = React.useState("");

  const [dataLogin, setDataLogin] = React.useState("");

  const [localData, setLocalData] = React.useState("");

  const [taskList, setTaskList] = React.useState([]);

  const [isLogin, setIsLogin] = React.useState();
  const [userName, setuserName] = React.useState(localStorage.getItem("name"));

  const [refCodeURL, setRefCodeURL] = React.useState("");
  const [refCode, setRefCode] = React.useState(localStorage.getItem("refCode"));
  //const user = JSON.parse(localStorage.getItem('dataLogin'))
  const [loginURL, setLoginURL] = React.useState("");

  const [refLink, setRefLink] = React.useState(localStorage.getItem("refLink"));

  const [totalEntries, setTotalEntries] = React.useState(0);

  const [isWalletDone, setIsWalletDone] = React.useState(localStorage.getItem("wallet"));

  const baseURL = "https://www.metaflokirushwhitelist.com/admin/api/users";
  const loginBaseURL = "https://www.metaflokirushwhitelist.com/admin/api/loginuserswidget";
  const taskBaseURL = "https://www.metaflokirushwhitelist.com/admin/api/socialListing";
  const totalEntriesBaseURL = "https://www.metaflokirushwhitelist.com/admin/api/totalEntries";

  useEffect(() => {
    setIsLogin(JSON.parse(localStorage.getItem('dataLogin')));
  }, []);

  React.useEffect(() => {

    // tooltipTrigger();
    console.log('testlocal smart: ', localStorage.getItem('testlocal'));

    setIsWalletDone(localStorage.getItem("wallet"));
    // posdata();
    // getdata();
    getTaskdata();
    getTotalEntries();
    // const loginURL = window.location.href;
    // console.log(loginURL);
    // getReferalLink()
    console.log('refcode dalam state effect',refCode);

    const link = window.location.origin;
    const search = window.location.search;
    const params = new URLSearchParams(search);
    const refcode = params.get('refcode');
    console.log(search);
    console.log('refcode', refcode);
    console.log('Link', link);

    const loginURL = link +'/'+ search;
    setLoginURL(loginURL)
    console.log('loginURL: ', loginURL);

    setRefCodeURL(refcode)
  }, []);

  const [date, setDate] = React.useState(0);
  const [days, setDays] = React.useState(0);
  const [hours, setHours] = React.useState(0);
  const [minutes, setMinutes] = React.useState(0);
  const [seconds, setSeconds] = React.useState(0);

  useEffect(() => {
    // Set the date we're counting down to
    let countDownDate = new Date("Jan 30, 2022 16:09:0").getTime();

    // Update the count down every 1 second
    let x = setInterval(function () {
      // Get today's date and time
      let now = new Date().getTime();

      // Find the distance between now and the count down date
      let distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      let days = Math.floor(distance / (1000 * 60 * 60 * 24));
      let hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);
      setDate(seconds);
      setDays(days);
      setHours(hours);
      setMinutes(minutes);
      setSeconds(seconds);
      // Output the result in an element with id="demo"
      // document.getElementById("demo").innerHTML = days + "d " + hours + "h "
      // + minutes + "m " + seconds + "s ";

      // If the count down is over, write some text
      if (distance < 0) {
        clearInterval(x);


       //setIsLogin(false); use when countdown start
       // alert('Time is Up!');
       setDate(0);
       setDays(0);
       setHours(0);
       setMinutes(0);
       setSeconds(0);
        // document.getElementById("demo").innerHTML = "EXPIRED";
      }
    }, 1000);
  }, []);

  function getTaskdata() {
    const config = {
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        responseType: "json",
      };
    
    axios.get(taskBaseURL, config).then((response) => {
        
        console.log("respon get task list", response.data);

        const responseAPI = response.data;
        setTaskList(responseAPI.socialdata);
        console.log(responseAPI.socialdata);
    });

    // console.log("panggil", taskList.socialdata);
  }

  function getTotalEntries() {
    const config = {
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        responseType: "json",
      };
    
    axios.get(totalEntriesBaseURL, config).then((response) => {
        
        console.log("respon get total entries: ", response.data);

        const responseAPI = response.data;
        setTotalEntries(responseAPI.totalEntries);
        console.log(responseAPI.totalEntries);
    });

    // console.log("panggil", taskList.socialdata);
  }

  // function getdata() {
  //   const config = {
  //     headers: { "Content-Type": "application/x-www-form-urlencoded" },
  //     responseType: "json",
  //   };

  //   axios.get(baseURL, config).then((response) => {
  //     setPost(response.data);
  //     console.log("respon get", post);
  //   });
  // }

  // function posdata() {
  //   const params = new URLSearchParams();
  //   params.append("email", email);
  //   params.append("fname", name);

  //   const config = {
  //     headers: { "Content-Type": "application/x-www-form-urlencoded" },
  //     responseType: "json",
  //   };

  //   axios.post(
  //       loginBaseURL,
  //       params,
  //       config
  //     )
  //     .then((response) => {
  //       console.log("respon post", response.data);
  //     });
  // }

  function login(event) {

             if(name === ''){
               alert('name is required')
             }else if(email === ''){
               alert('email is required')
             }else{
               const params = new URLSearchParams();
               params.append("email", email);
               params.append("fname", name);
              //  params.append("refcodereward", refCode);
               params.append("refcode", refCodeURL);
              //  refCodeURL
               
               const config = {
                 headers: { "Content-Type": "application/x-www-form-urlencoded" },
                 responseType: "json",
               };
   
               axios.post(
                   loginBaseURL,
                   params,
                   config
                 )
                 .then((response) => {
                   
                  //setDataLogin(response.data.datauser)
                  console.log("respon post", response.data.datauser.isLoggedIn);
                  //  window.localStorage.setItem("dataLogin", JSON.stringify(response.data.datauser));
                  //  setLocalData(response.data.datauser);
                  localStorage.setItem("dataLogin", JSON.stringify(response.data.datauser.isLoggedIn));
                  localStorage.setItem("name", JSON.stringify(response.data.datauser.name));
                  localStorage.setItem("idusername", response.data.datauser.userId);
                  localStorage.setItem("refCode", response.data.datauser.id_referal_code);
                 
              
   
                // console.log("respon localstorage get item", setlogin);
                  //const setlogin = localStorage.getItem('dataLogin');
                  setIsLogin(JSON.parse(localStorage.getItem('dataLogin')));
                 
                  //console.log(dataLogin);
                  setuserName(localStorage.getItem('name'));
                  setRefCode(localStorage.getItem("refCode"));

                  // GENERATE LINK
                  const link = window.location.origin;
                  localStorage.setItem("refLink", link + '/?refcode=' + response.data.datauser.id_referal_code)
                  setRefLink(localStorage.getItem("refLink"));
                  // console.log('refLink:'  , refLink);
                  
                 });
   
                 // const data = window.localStorage.getItem(dataLogin);
   
         }
   
         
       
     }

  function sentDataAkordionasdasd(idUser, taskid, taskinputklik, username) {
    alert(username)
   console.log(idUser);
   console.log(taskid);
   console.log(taskinputklik);
   console.log(username);

   const postUsername = "https://petoverse.io/whitelist/api/usertasksocial"

   const params = new URLSearchParams();
           params.append("iduser", idUser);
           params.append("idsocialmedia", taskid);
           params.append("inputklik", taskinputklik);
           params.append("usernamesocremark", username);

           const config = {
             headers: { "Content-Type": "application/x-www-form-urlencoded" },
             responseType: "json",
           };

   // axios.post(
   //   postUsername,
   //   params,
   //   config
   // )
   // .then((response) => {
   
   //   console.log("respon", response);
   
   // });
 }

  
  function logout() {
   // localStorage.removeItem("dataLogin");
    localStorage.clear();
    setIsLogin(false);
    //window.location.reload();
   // alert('cek')
  }

  // const [isCopied, setIsCopied] = React.useState(false)

  function copyFunction() {
    /* Get the text field */
    var copyText = document.getElementById("referal-link");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /* For mobile devices */

    /* Copy the text inside the text field */
    navigator.clipboard.writeText(copyText.value);

    /* Alert the copied text */
    // alert("Copied the text: " + copyText.value);

    var tooltip = document.getElementById("copy-icon");
    // tooltip.innerHTML = "Copied: " + copyText.value;
    // tooltip.classList.replace('bi-clipboard', 'bi-clipboard-check');
    tooltip.classList.replace('bi-clipboard', 'bi-check2');
    // tooltip.setAttribute('title', 'Copied')
    // setIsCopied(true);

  }

  function outFunc() {
    var tooltip = document.getElementById("copy-icon");
    // tooltip.classList.replace('bi-clipboard-check', 'bi-clipboard');
    tooltip.classList.replace('bi-check2', 'bi-clipboard');
    
  }

  // function tooltipTrigger() {
  //   var tooltipTriggerList = [].slice.call(
  //     document.querySelectorAll('[data-bs-toggle="tooltip"]')
  //   );
  //   tooltipTriggerList.map(function (tooltipTriggerEl) {
  //     return new Tooltip(tooltipTriggerEl);
  //   });
  // }

 

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.querySelectorAll('.needs-validation')

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms)
    .forEach(function (form) {
      form.addEventListener('click', function (event) {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        form.classList.add('was-validated')
      }, false)
    })
})()


  return (
    <div id="background-smart-content">

      {/* <!-- The video --> */}
      <video autoPlay muted loop id="myVideo">
        <source src="../FlokiRushBG.mp4" type="video/mp4" />
      </video>

      <div className="container">
        <div className="head">
        <div>

    </div>
          <div className="row">
            {/* <div className="col-4 info info-1">
              <div className="col-md-12">

                
                <h3>0</h3>
              </div>
              <div className="col-md-12 ">My Entries</div>
            </div> */}
            <div className="col-6 info info-2">
              <div className="col-md-12">
                <h3>{totalEntries}</h3>
              </div>
              <div className="col-md-12">All Entries</div>
            </div>
            <div className="col-6 info info-3">
              <div className="col-md-12">
                <h3>{days} Days</h3>
              </div>
              <div className="col-md-12">
                {hours}:{minutes}:{seconds}
              </div>
            </div>
          </div>
        </div>
        <div className="media">
          <div className="row">
            <div className="col-md-12">
              <iframe
                width="100%"
                height="500"
                src="https://www.youtube.com/embed/fFDAggAyE2Y"
                title="YouTube video player"
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              ></iframe>
            </div>
          </div>
        </div>

        <div className="text-title mt-3 px-3">
          <h3>Meta Floki Rush – The greatest blockchain-based play-to-earn game on the Binance Smart Chain! </h3>
        </div>
        <div className="text-deskripsi px-3">
          <p>Join Meta Floki Rush and be a part of Meta Floki Rush Rise to stardom! Complete the tasks below to participate in our Whitelist Competition!</p>
          <p>This year’s rise in the crypto market has led to the rise of a new and wonderful world. 
            The play-to-earn world has seen multiple cryptocurrencies exceed $1bn market cap valuations. 
            META FLOKI RUSH ($FLOKIRUSH) is an all-inclusive ecosystem for gamers and traders powered by Defi and NFT. 
            We aim to not only provide a place for fun but at the same time serve as a platform that generates real income for the holders. 
            To achieve this goal, the META FLOKI RUSH ecosystem combines the elements of gaming and crypto, creating an enjoyable and rewarding experience for our players.</p>
          <p>We are inspired by Elon’s Shiba Inu pup and we want to combine a ginormous cuteness with a fun game hence the META FLOKI RUSH is created. 
            We aim to give DogeCoin rewards to our holders. Especially with the rise of interest in crypto, 
            we believe that META FLOKI RUSH will be a solution for this. The concept of playing a game that people are already 
            familiar with will help them familiarize themselves with crypto while also getting profit from the token itself.</p>
          <p>Whitelisted slots do have the preferences of being able to buy before the majority of interests. The presale for Rari Meta Floki Rush will be hosted on pinksale.finance and whitelisted addresses can buy 5 minutes early before the sale goes public. As we expect our sales to rapidly fill up, this assures you an early spot for this project.</p>

          <button href="#" className="btn btn-link">Read our Whitepaper!</button>

          <p>Complete all tasks below and leave your wallet address ( BEP20 / BSC ) to be whitelisted for Meta Floki Rush presale.</p>
        </div>
        <div>
      {(() => {
        if (isLogin && isLogin === true) {
          return (
            // Logged In View
       <div className="px-3">

       <div class="alert alert-success d-flex align-items-center" role="alert">
         {/* <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg> */}
         <i class="bi bi-check-circle me-2"></i>
         <div>
           Welcome, {userName}. 
         </div>
       </div>
       {isWalletDone?
         <div>
           <p>Share With Your Friends to Unlock Extra Entries</p>
       <label className="form-label">Get <span className="badge bg-success" >+3</span> extra entries for anyone that enters via this link:</label>
       <div class="container input-group mb-3 px-">
       {/* <span class="input-group-text">http://localhost:3000/</span> */}
         <input 
           readonly type="text" class="form-control" placeholder={refLink} 
           aria-label="Referral Link" aria-describedby="referal-link" 
           id="referal-link" value={refLink}/>

           {/* {isCopied? */}
             <button class="btn btn-outline-secondary" type="button" id="copy-button" 
               onClick={copyFunction} onMouseOut={setInterval(outFunc, 3000)}
               data-bs-toggle="tooltip" data-bs-placement="top" title="Copy Link"
               ><i class="bi bi-clipboard" id="copy-icon"></i></button>
             {/* :
             <button class="btn btn-outline-secondary" type="button" id="copy-button" 
               onClick={copyFunction}
               data-bs-toggle="tooltip" data-bs-placement="top" title="Copied"
               ><i class="bi bi-facebook"></i></button>
           } */}
         
       </div>
         </div>
       :null}
       

       {/* <p>Share to:</p>
         <button type="button" class="btn btn-secondary" data-bs-toggle="tooltip" data-bs-placement="top" title="Tooltip on top">
           Tooltip on top
         </button>
         <div class="alert alert-warning alert-dismissible fade show" role="alert">
           <strong>Holy guacamole!</strong> You should check in on some of those fields below.
           <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
         </div>
       <p>Note: To be credited for a referral, your friends must login.</p> */}

       <button type="button" class="btn btn-light me-2" onClick={logout}><i class="bi bi-box-arrow-right"></i> Logout</button>
     </div>
         )
      
        } else {
          return (
            <div className="login-section px-3">
            <form className="needs-validation">
  
              <div className="mb-3">
                <label for="fullName" className="form-label">
                  Full Name <span>*</span>
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="fname"
                  name="fname"
                  aria-describedby="emailHelp"
                  onChange={e => setName(e.target.value)}
                  required
                />
                {/* <!-- <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> --> */}
              </div>
              <div className="mb-3">
                <label for="emailAddress" className="form-label">
                  Email address <span>*</span>
                </label>
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  name="email"
                  onChange={e => setEmail(e.target.value)}
                  aria-describedby="emailHelp"
                  required
                />
                {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
              </div>
              <center>
                <button
                  type="button"
                  className="btn btn-primary col-12"
                  onClick={login}
                >
                  Login
                </button>
              </center>
            </form>
            <div className="alert alert-warning info mt-3" role="alert">
              <i className="bi bi-lock-fill"></i> Please Login First! 
            </div>
          </div>
          )
        }
      })()}
    </div>
  
      
        <div className="task-content mt-3">
          <ul className="nav nav-tabs nav-justified" id="myTab" role="tablist">
            <li className="nav-item" role="presentation">
              {/* <button
                className="nav-link active"
                id="home-tab"
                data-bs-toggle="tab"
                data-bs-target="#home"
                type="button"
                role="tab"
                aria-controls="home"
                aria-selected="true"
              >
                Earn Entries
              </button> */}
            </li>
            <li className="nav-item" role="presentation">
              {/* <button
                className="nav-link"
                id="profile-tab"
                // data-bs-toggle="tab" disabled tab
                data-bs-target="#profile"
                type="button"
                role="tab"
                aria-controls="profile"
                aria-selected="false"
              >
                My Entries
              </button> */}
            </li>
          </ul>
          <div className="tab-content" id="myTabContent">
            <div
              className="tab-pane fade show active"
              id="home"
              role="tabpanel"
              aria-labelledby="home-tab"
            >
              <div className="task-wrap">
                <div className="col-md-12">
                  
                {/* <TaskItem data = {taskList} /> */}
                    {/* {taskList.map(task => {
                        return task.social_med_name;
                    })} */}

                  
                
                  
                </div>
              </div>
            </div>
            <div
              className="tab-pane fade"
              id="profile"
              role="tabpanel"
              aria-labelledby="profile-tab"
            >
              <div className="card text-center mt-2 mb-2">
                <div className="card-header">Join Telegram Group</div>
                <div className="card-body">
                  <h5 className="card-title">@hgsavdjhsv</h5>
                  {/* <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
                            <a href="#" className="btn btn-primary">Go somewhere</a> */}
                </div>
                <div className="card-footer text-muted">
                   days ago (20/01/2022 12:00:00)
                </div>
              </div>
            </div>
          </div>
        </div>
        {isLogin ?

         <Akordion />
        :null
        
        }
       
        

        <div className="info mt-3">
          <div className="row">
            <div className="col-6">
              <div className="col-md-12">1</div>
              <div className="col-md-12">Winner</div>
            </div>
            <div className="col-6">
              <div className="col-md-12">01-21-2022</div>
              <div className="col-md-12">End Date</div>
            </div>
            {/* <div className="col-4">
              <div className="col-md-12">Rules</div>
              <div className="col-md-12">Show</div>
            </div> */}
          </div>
        </div>

        <div className="footer mt-3">
          <footer style={myFooter}>
          {/* style={`backgroundImage: url('img/bg-footer.png')`} */}
            <div className="col-md-12" > 
              <img src="img/logo/logo_metalokirush.png" className=" img-fluid logo-bawah mx-auto mb-4" alt="" />
            </div>
            <div className="footer-content">
              
              {/* <h3>Meta Floki Rush</h3> */}
              {/* <p>
                Powered by <a href="#">SmartWidget</a>
              </p> */}

            </div>
          </footer>
        </div>
      </div>
    </div>
  );
};
