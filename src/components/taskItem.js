import React, { useState, useEffect } from "react";

import axios from "axios";

export const TaskItem = (props) => {
  
  const [idUser, setIdUser] = React.useState(localStorage.getItem('idusername'));
  const data = props.data;
  console.log(idUser);
  console.log('id user name tt', localStorage.getItem('idusername'));

  function sentData(idUser, taskid, taskinputklik, username) {
    // alert(username)
    console.log(idUser);
    console.log(taskid);
    console.log(taskinputklik);
    console.log(username);

    const postUsername = "https://petoverse.io/whitelist/api/usertasksocial"

    const params = new URLSearchParams();
            params.append("iduser", idUser);
            params.append("idsocialmedia", taskid);
            params.append("inputklik", taskinputklik);
            params.append("usernamesocremark", username);

            const config = {
              headers: { "Content-Type": "application/x-www-form-urlencoded" },
              responseType: "json",
            };

    // axios.post(
    //   postUsername,
    //   params,
    //   config
    // )
    // .then((response) => {
    
    //   console.log("respon", response);
    
    // });
  }

  return (
    <div>
      {data.length > 0 && data.map(task => {
        return (
          // <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
          //   Launch static backdrop modal
          // </button>
          <div>
          <div className="row" data-bs-toggle="modal" data-bs-target={`#staticBackdrop-${task.id}`}>
            <div className="task">
              <div className="task-icon">
                <i className={`bi bi-${task.icon}`}></i>
              </div>

              <div className="task-text">{task.social_med_name}</div>
              <div className="task-point">+5</div>
            </div>
          </div>
          <div class="modal fade" id={`staticBackdrop-${task.id}`} data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
              <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Username</label>
                <input type="email" class="form-control" id="Username" placeholder="name@example.com " />
              </div>
              </div>
              <div class="modal-footer">
                {/* <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> */}
                <button type="button" class="btn btn-primary" 
                // onClick={sentData(idUser, task.id, task.inputklik, "tasdja")}
                >Confirm</button>
              </div>
            </div>
          </div>
        </div>
        </div>
        );
        
      })}
      

    </div>
  );
};
