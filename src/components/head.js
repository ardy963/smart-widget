import React, { useState, useEffect } from "react";

import './head.css';

export const Head = (props) => {
  return (
    <div>
      <div class="container">
        <div class="head">
          <div class="row">
            <div class="col-4 info-1">
              <div class="col-md-12">0</div>
              <div class="col-md-12">My Entries</div>
            </div>
            <div class="col-4 info">
              <div class="col-md-12">1000</div>
              <div class="col-md-12">All Entries</div>
            </div>
            <div class="col-4 info">
              <div class="col-md-12">1 Day</div>
              <div class="col-md-12">07:30:00</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
