//import logo from './logo.svg';
// import './App.css';

// import { About } from "./components/about";

// import { Gameplay } from "./components/gameplay";
// import { Header } from "./components/header";
// import { Navigationtop } from "./components/navigationtop";
// import { Vr } from "./components/vr";
// import { Whypetoverse } from "./components/whypetoverse";

import { Head } from "./components/head";
import { Media } from "./components/media";
import { Text } from "./components/text";
import { LoginSection } from "./components/login-section";
import { Content } from "./components/content";
import { InfoBot } from "./components/info-bot";

import { Footer } from "./components/footer";

import { SmartContent } from "./components/smartContent";

function App() {
  return (
    <div>
     
      {/* <Head/>
      <Media/>
      <Text/>
      <LoginSection/>
      <Content/>
      <InfoBot/>
      <Footer/> */}

      <SmartContent/>
    </div>
  );
}

export default App;
